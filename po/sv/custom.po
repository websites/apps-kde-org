#
# SPDX-FileCopyrightText: 2024, 2025 Stefan Asserhäll <stefan.asserhall@gmail.com>
msgid ""
msgstr ""
"Project-Id-Version: websites-apps-kde-org 1.0\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-11-06 01:51+0000\n"
"PO-Revision-Date: 2025-01-13 19:24+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@gmail.com>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 24.12.0\n"

#: content/custom/itinerary.en.md:7
msgid "![](https://apps.kde.org/app-icons/org.kde.itinerary.svg)"
msgstr "![](https://apps.kde.org/app-icons/org.kde.itinerary.svg)"

#: content/custom/itinerary.en.md:9
msgid "KDE Itinerary"
msgstr "KDE-resplan"

#: content/custom/itinerary.en.md:11
msgid ""
"KDE Itinerary is a digital travel assistant that protects your privacy. It "
"makes collecting all the information about your travel inside a single "
"application easy and straightforward. KDE Itinerary is available for [Plasma "
"Mobile](https://plasma-mobile.org/) and Android."
msgstr ""
"KDE-resplan är en digital reseassistent som skyddar din integritet. Det gör "
"det enkelt och rättframt att samla in all information om en resa i ett enda "
"program. KDE-resplan är tillgänglig för [Plasma Mobil](https://plasma-mobile."
"org/) och Android."

#: content/custom/itinerary.en.md:17
msgid "Store your reservations"
msgstr "Lagra dina bokningar"

#: content/custom/itinerary.en.md:19
msgid ""
"Store all the information about your reservations in Itinerary. This "
"includes QR-codes, check-in times, arrivial times, real-time delays, seat "
"reservations, coach layout, and more."
msgstr ""
"Lagra all information om dina bokningar i Resplan. Det inkluderar QR-koder, "
"incheckningstider, ankomsttider, realtidsförseningar, platsreservationer, "
"bussplatser med mer."

#: content/custom/itinerary.en.md:21
msgid ""
"Itinerary supports train, bus and flight bookings, as well as hotel, "
"restaurant, event and rental car reservations. Traveling in a group? Not a "
"problem, Itinerary supports multi-traveler bookings."
msgstr ""
"Resplan stöder tåg-, buss- och flygbokningar samt bokningar av hotell, "
"restauranger, evenemang och hyrbilar. Resa i grupp? Inget problem, Resplanen "
"stöder bokningar för flera resenärer."

#: content/custom/itinerary.en.md:27
msgid "Local first"
msgstr "Lokal först"

#: content/custom/itinerary.en.md:29
msgid ""
"Itinerary automatically extracts booking data from various input formats. "
"It's all performed locally on **your** device and your data is not sent to "
"any remote servers."
msgstr ""
"Resplan extraherar automatiskt bokningsdata från olika inmatningsformat. Det "
"hela utförs lokalt på **din** dator och data skickas inte till några "
"fjärrservrar."

#: content/custom/itinerary.en.md:31
msgid ""
"This works best when using [KMail](https://kontact.kde.org/components/"
"kmail/) to extract tickets from your email and then [KDE Connect](https://"
"kdeconnect.kde.org) to transfer tickets to your phone. This also works great "
"with [Nextcloud Mail](https://apps.nextcloud.com/apps/mail) and [DavDroid]"
"(https://f-droid.org/en/packages/at.bitfire.davdroid/) to sync your tickets "
"from [Nextcloud](https://nextcloud.com/)."
msgstr ""
"Det fungerar bäst när du använder [Kmail](https://kontact.kde.org/components/"
"kmail/) för att extrahera biljetter från e-post och sedan [KDE-anslut]"
"(https://kdeconnect.kde.org) för att överföra biljetter till din telefon. "
"Det fungerar också utmärkt med [Nextcloud Mail](https://apps.nextcloud.com/"
"apps/mail) och [DavDroid](https://f-droid.org/en/packages/at.bitfire."
"davdroid/) för att synkronisera biljetter från [Nextcloud](https://nextcloud."
"com/)."

#: content/custom/itinerary.en.md:33
msgid ""
"![KMail ticket extraction showing a train trip from Berlin to Tübingen]"
"(https://kde.org/for/travelers/kmail.png)"
msgstr ""
"![Extrahering av biljetter från Kmail som visar en tågresa från Berlin till "
"Tübingen](https://kde.org/for/travelers/kmail.png)"

#: content/custom/itinerary.en.md:39
msgid "Add your connections"
msgstr "Lägg till dina anslutningar"

#: content/custom/itinerary.en.md:41
msgid ""
"Aside from finding reservations automatically in your email, Itinerary lets "
"you add train trips manually to your journey, find alternative connections "
"if your train is cancelled, or, for some providers, import your train trip "
"directly from your reservation number."
msgstr ""
"Förutom att hitta bokningar automatiskt i e-post, låter Resplan dig lägga "
"till tågresor manuellt i resan, hitta alternativa anslutningar om tåg ställs "
"in eller, för vissa transportföretag, importera din tågresa direkt från ditt "
"bokningsnummer."

#: content/custom/itinerary.en.md:47
msgid "Find your way"
msgstr "Hitta rätt"

#: content/custom/itinerary.en.md:49
msgid ""
"Powered by [OpenStreetMap](https://www.openstreetmap.org), the indoor map at "
"train stations or airports can be a life saver. Use Itinerary to locate your "
"platform is, and, if you have seat reservation and the train layout is "
"available, it can even show you exactly which platform section is best for "
"you."
msgstr ""
"Med användning av [OpenStreetMap](https://www.openstreetmap.org), kan "
"inomhuskartan på tågstationer eller flygplatser vara en livräddare. Använd "
"Resplan för att lokalisera din plattform, och om du har platsreservation och "
"tågvagnar är tillgängliga kan den till och med visa dig exakt vilken "
"plattformssektion som är bäst för dig."

#: content/custom/itinerary.en.md:51
msgid ""
"![Train station map in KDE Itinerary, highlighting relevant platform "
"sections.](https://www.volkerkrause.eu/assets/posts/139/kde-itinerary-"
"platform-section-highlighting.jpg)"
msgstr ""
"![Tågstationskarta i KDE:s Resplan, som markerar relevanta "
"plattformssektioner.](https://www.volkerkrause.eu/assets/posts/139/kde-"
"itinerary-platform-section-highlighting.jpg)"

#: content/custom/itinerary.en.md:53
msgid ""
"The indoor map also shows you which shops and resturantes are currently "
"open, which elevator is broken (yet again!), where the toilets are and where "
"the correct exit is."
msgstr ""
"Inomhuskartan visar också vilka butiker och restauranger som har öppet just "
"nu, vilken hiss som är trasig (igen), var toaletterna finns och var rätt "
"utgång är."

#: content/custom/itinerary.en.md:59
msgid "Real time"
msgstr "Realtid"

#: content/custom/itinerary.en.md:61
msgid ""
"It is rare that a train or bus departs or arrives on time. Itinerary keeps "
"you updated when delays are announced."
msgstr ""
"Det är sällsynt att ett tåg eller buss avgår eller anländer i tid. Resplan "
"håller dig uppdaterad när förseningar meddelas."

#: content/custom/itinerary.en.md:63
msgid ""
"On supported train and long distance buses, Itinerary will also use the "
"onboard APIs to fetch the current live status of the vehicle and keep you "
"updated on your current position and any announcements."
msgstr ""
"På tåg- och långdistansbussar som stöds använder Itinerary också "
"programmeringsgränssnitt ombord för att hämta fordonets aktuella status och "
"håller dig uppdaterad om nuvarande position och eventuella meddelanden."

#: content/custom/kleopatra.en.md:7
msgid "![](https://apps.kde.org/app-icons/org.kde.kleopatra.svg)"
msgstr "![](https://apps.kde.org/app-icons/org.kde.kleopatra.svg)"

#: content/custom/kleopatra.en.md:9
msgid "Kleopatra"
msgstr "Kleopatra"

#: content/custom/kleopatra.en.md:11
msgid ""
"Kleopatra is an open-source certificate manager and graphical front-end for "
"cryptographic services, primarily designed to handle OpenPGP and S/MIME "
"(X.509) certificates. Part of the KDE ecosystem, it provides an accessible "
"interface for managing encryption keys, signing and verifying data, and "
"encrypting or decrypting files and emails."
msgstr ""
"Kleopatra är en certifikathanterare med öppen källkod och ett grafiskt "
"gränssnitt för kryptografiska tjänster, främst konstruerat för att hantera "
"OpenPGP- och S/MIME (X.509)-certifikat. Som del av KDE:s ekosystem, "
"tillhandahåller den ett lättillgängligt gränssnitt för hantering av "
"krypteringsnycklar, signering och verifiering av data, och kryptering eller "
"avkodning av filer och e-post."

#: content/custom/kleopatra.en.md:21
msgid "Key Management"
msgstr "Nyckelhantering"

#: content/custom/kleopatra.en.md:23
msgid ""
"**Create, Import, Export Keys**: Users can create new public and private "
"encryption keys, import existing ones, and export keys to share with others."
msgstr ""
"**Skapa, importera, exportera nycklar**: Användare kan skapa nya öppna och "
"privata krypteringsnycklar, importera befintliga och exportera nycklar för "
"att dela med andra."

#: content/custom/kleopatra.en.md:25
msgid ""
"**Key Servers**: Kleopatra can connect to public key servers, allowing users "
"to look up and import public keys from others or upload their own public "
"keys to make them available globally."
msgstr ""
"**Nyckelservrar**: Kleopatra kan ansluta till öppna nyckelservrar, så att "
"användare kan slå upp och importera öppna nycklar från andra eller ladda upp "
"sina egna öppna nycklar för att göra dem tillgängliga globalt."

#: content/custom/kleopatra.en.md:28
msgid ""
"**LDAP and WKD**: Aside from key servers, Kleopatra can also connect to LDAP "
"and WKD (Web Key Directory) servers and fetch certificates from there."
msgstr ""
"**LDAP och WKD**: Förutom nyckelservrar kan Kleopatra också ansluta till "
"LDAP- och WKD-servrar (Web Key Directory) och hämta certifikat därifrån."

#: content/custom/kleopatra.en.md:30
msgid ""
"**Trust and Expiry Management**: Users can set trust levels for keys and "
"manage expiration dates to ensure only trusted keys are in use."
msgstr ""
"**Rättighets- och utgångshantering**: Användare kan ställa in "
"pålitlighetsnivåer för nycklar och hantera utgångsdatum för att säkerställa "
"att endast pålitliga nycklar används."

#: content/custom/kleopatra.en.md:37
msgid "Data Encryption and Decryption"
msgstr "Datakryptering och avkodning"

#: content/custom/kleopatra.en.md:39
msgid ""
"**Encrypt Files and Texts**: With Kleopatra, users can encrypt files and "
"texts to protect the content and ensure it is only accessible by recipients "
"with the correct private key."
msgstr ""
"**Kryptera filer och texter**: Med Kleopatra kan användare kryptera filer "
"och texter för att skydda innehållet och säkerställa att det endast är "
"tillgängligt för mottagare med rätt privat nyckel."

#: content/custom/kleopatra.en.md:42
msgid ""
"**Decrypt Content**: It supports decryption for files, text and emails, "
"making it easy for users to access data they received securely."
msgstr ""
"**Avkoda innehåll**: Det stöder avkodning av filer, text och e-post, vilket "
"gör det enkelt för användare att komma åt data de tagit emot på ett säkert "
"sätt."

#: content/custom/kleopatra.en.md:45
msgid ""
"**Digital Signatures**: Kleopatra lets users sign files or notes with their "
"private key, which helps verify the authenticity of the sender and integrity "
"of the data."
msgstr ""
"**Digitala signaturer**: Kleopatra låter användare signera filer eller "
"anteckningar med sin privata nyckel, vilket hjälper till att verifiera "
"avsändarens äkthet och dataintegriteten."

#: content/custom/kleopatra.en.md:53
msgid "Support for OpenPGP and S/MIME"
msgstr "Stöd för OpenPGP och S/MIME"

#: content/custom/kleopatra.en.md:55
msgid ""
"**OpenPGP**: Commonly used in personal and enterprise settings for "
"encrypting and signing emails and files. Kleopatra provides full support for "
"creating, managing, and using OpenPGP certificates."
msgstr ""
"**OpenPGP**: Ofta använd i personliga och företagsmiljöer för att kryptera "
"och signera e-post och filer. Kleopatra ger fullt stöd för att skapa, "
"hantera och använda OpenPGP-certifikat."

#: content/custom/kleopatra.en.md:57
msgid ""
"**S/MIME**: Often used in enterprise environments for email security. "
"Kleopatra supports S/MIME certificates, enabling it to handle encryption and "
"signing based on the [CMS](https://en.m.wikipedia.org/wiki/"
"Cryptographic_Message_Syntax) standard."
msgstr ""
"**S/MIME**: Ofta använt i företagsmiljöer för e-postsäkerhet. Kleopatra "
"stöder S/MIME-certifikat, vilket gör att det kan hantera kryptering och "
"signering baserat på standarden [CMS](https://en.m.wikipedia.org/wiki/"
"Cryptographic_Message_Syntax)."

#: content/custom/kleopatra.en.md:63
msgid "Support for OpenPGP and PIV Smartcards"
msgstr "Stöd för OpenPGP och PIV smartkort"

#: content/custom/kleopatra.en.md:65
msgid ""
"**Manage your Smartcard**: Kleopatra lets users change their the PIN and PUK "
"code of the smartcard, change metadata like the cardholder's name, copy "
"existing private keys to the card and even generate directly new keys on the "
"card."
msgstr ""
"**Hantera smartkort**: Kleopatra låter användare ändra PIN- och PUK-koden "
"för smartkort, ändra metadata som kortinnehavarens namn, kopiera befintliga "
"privata nycklar till kortet och till och med generera nya nycklar direkt på "
"kortet."

#: content/custom/kleopatra.en.md:66
msgid ""
"**Authentification Keys**: Create and use authentification keys from "
"smartcards for SSH logins and more."
msgstr ""
"**Behörighetskontrollnycklar**: Skapa och använd behörighetskontrollnycklar "
"från smartkort för SSH-inloggningar med mera."

#: content/custom/kleopatra.en.md:67
msgid ""
"**Compatibility**: Kleopatra is compatible with Yubikey, [Gnuk](https://www."
"fsif.org/doc-gnuk/) and other cards without the need for extra device "
"drivers."
msgstr ""
"**Kompatibilitet**: Kleopatra är kompatibel med Yubikey, [Gnuk](https://www."
"fsif.org/doc-gnuk/) och andra kort utan behov av extra enhetsdrivrutiner."

#: content/custom/kleopatra.en.md:73
msgid "Integration and Usability"
msgstr "Integrering och användbarhet"

#: content/custom/kleopatra.en.md:75
msgid ""
"**Gpg4win**: On Windows, Kleopatra is bundled as part of the [Gpg4win suite]"
"(https://gpg4win.org), making it the primary tools for managing GnuPG (GPG) "
"encryption on Windows. Kleopatra is also bundled as part of [GnuPG Desktop®️]"
"(https://gnupg.com/gnupg-desktop.html) which comes with enterprise support "
"and as part of [GnuPG VS-Desktop®️](https://gnupg.com/gnupg-vs-desktop.html) "
"which is compliant for use with EU and NATO RESTRICTED material and the "
"German VS-NfD."
msgstr ""
"**Gpg4win**: På Windows ingår Kleopatra som en del av [Gpg4win-sviten]"
"(https://gpg4win.org), vilket gör det till det primära verktyget för att "
"hantera GnuPG-kryptering (GPG) på Windows. Kleopatra ingår också som en del "
"av [GnuPG Desktop®️](https://gnupg.com/gnupg-desktop.html) som levereras med "
"företagssupport och som en del av [GnuPG VS-Desktop®️](https://gnupg. com/"
"gnupg-vs-desktop.html) som är kompatibel för användning med EU och NATO "
"RESTRICTED material och tyska VS-NfD."

#: content/custom/kleopatra.en.md:76
msgid ""
"**Seamless Integration with KDE Applications**: For Linux users, Kleopatra "
"integrates well within the KDE Plasma, working alongside other KDE apps like "
"[KMail](https://kontact.kde.org/components/kmail/) for secure document "
"handling."
msgstr ""
"**Sömlös integrering med KDE-program**: För Linux-användare integreras "
"Kleopatra väl i KDE Plasma och arbetar tillsammans med andra KDE-program som "
"[Kmail](https://kontact.kde.org/components/kmail/) för säker "
"dokumenthantering."

#: content/custom/kleopatra.en.md:77
msgid ""
"**File Encryption from File Managers**: It enables direct encryption, "
"signing, and verification of files from file managers, making it very user-"
"friendly for everyday use."
msgstr ""
"**Filkryptering från filhanterare**: Det möjliggör direkt kryptering, "
"signering och verifiering av filer från filhanterare, vilket gör det mycket "
"användarvänlig för dagligt bruk."

#: content/custom/kleopatra.en.md:83
msgid "User-Friendly Interface"
msgstr "Användarvänligt gränssnitt"

#: content/custom/kleopatra.en.md:85
msgid ""
"**Graphical Interface**: Kleopatra provides a GUI to manage cryptographic "
"tasks, which is beneficial for users unfamiliar with command-line tools like "
"GPG."
msgstr ""
"**Grafiskt gränssnitt**: Kleopatra tillhandahåller ett grafiskt "
"användargränssnitt för att hantera kryptografiska uppgifter, vilket är "
"fördelaktigt för användare som inte är bekanta med kommandoradsverktyg som "
"GPG."

#: content/custom/kleopatra.en.md:86
msgid ""
"**Notepad**: Encrypt, decrypt and verify texts with the Notepad. Copy and "
"paste encrypted text to possibly insecure or unreliable messaging and chat "
"services."
msgstr ""
"**Anteckningar**: Kryptera, avkoda och verifiera texter med Anteckningar. "
"Kopiera och klistra in krypterad text till eventuellt osäkra eller "
"opålitliga meddelande- och chattjänster."

#: content/custom/kleopatra.en.md:87
msgid ""
"**Alerts**: Kleopatra notifies users about key status, trust levels, and any "
"potential issues, providing insights for users to manage security settings "
"appropriately."
msgstr ""
"**Varningar**: Kleopatra meddelar användarna om nyckelstatus, "
"pålitlighetsnivåer och eventuella problem, vilket ger användarna insikt i "
"att hantera säkerhetsinställningar på lämpligt sätt."

#: content/custom/kleopatra.en.md:88
msgid ""
"**Configuration**: Configure Kleopatra to your needs. kleopatra's "
"configurations can be centrally configured to meet organizational "
"requirements."
msgstr ""
"**Anpassning**: Anpassa Kleopatra efter dina behov. Kleopatras inställningar "
"kan anpassas centralt för att möta organisationens krav."

#, fuzzy
#~| msgid "![](https://apps.kde.org/app-icons/org.kde.itinerary.svg)"
#~ msgid ""
#~ "![](https://apps.kde.org/app-icons/org.kde.kleopatra.svg) {width=\"128\", "
#~ "height=\"128\"}"
#~ msgstr "![](https://apps.kde.org/app-icons/org.kde.itinerary.svg)"
