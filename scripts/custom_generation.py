# ŜPDX-FileCopyrightText: 2024 Carl Schwan <carl@carlschwan.eu>
# SPDX-License-Identifier: LGPL-2.1-or-later

import subprocess
import os

subprocess.run(['python3', 'main.py'], check=True)

os.environ["PACKAGE"] = 'websites-apps-kde-org'
os.system('git clone https://invent.kde.org/websites/hugo-i18n && pip3 install ./hugo-i18n')
os.system('hugoi18n compile po')  # compile translations in folder "po"
os.system('hugoi18n generate')
