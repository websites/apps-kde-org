# SPDX-FileCopyrightText: 2022 Phu Hung Nguyen <phu.nguyen@kdemail.net>
# SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
import pathlib

import yaml


class Unmaintained:
    # projects_w_repo, projects_wo_repo: use the 'Project.repo' field in the frontend
    with open('unmaintained_projects.yaml') as f_p:
        projects_w_repo, projects_wo_repo = yaml.safe_load_all(f_p)

    # if something is considered unmaintained and local, look for its appdata
    # and desktop files in this directory
    local_storage = 'appdata-unmaintained'

    # app ids to be considered unmaintained and local
    locally_stored_ids = (i.stem for i in pathlib.Path(local_storage).glob('*.appdata'))
    with_local_id = {i.split('.', 2)[2]: i for i in locally_stored_ids}.get
