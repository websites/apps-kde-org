# SPDX-FileCopyrightText: 2017-2020 Harald Sitter <sitter@kde.org>
# SPDX-FileCopyrightText: 2022 Phu Hung Nguyen <phu.nguyen@kdemail.net>
# SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL

from __future__ import annotations

import concurrent.futures
import logging
import os.path

import requests

from .unmaintained import Unmaintained


class Project:
    _cache = {}

    @classmethod
    def get(cls, repo: str):
        # Retrieve a single project. Use cache if applicable
        # For local unmaintained projects, repo is the same as 'identifier', but the 'repo' field is empty
        project = cls._cache.get(repo)
        if project:
            return project

        basename = os.path.basename(repo)
        if Unmaintained.with_local_id(basename):
            repo = '' if basename in Unmaintained.projects_wo_repo else repo
            data = {'repo': repo,
                    # '__do_not_use-legacy-projectpath': f'kde/old/{basename}',
                    'identifier': basename,
                    'bug_submit': '',
                    'bug_database': '',
                    'i18n': None}
        else:
            data = requests.get(f'https://projects.kde.org/api/v1/project/{repo}').json()
        project = Project(repo, data)
        cls._cache[repo] = project
        return project

    @classmethod
    def list_repos(cls) -> list:
        return requests.get('https://projects.kde.org/api/v1/projects').json()

    @classmethod
    def gen_cache(cls, repos: list[str]):
        logging.info('Getting info of all projects at once')
        # Use a with statement to ensure threads are cleaned up promptly
        with concurrent.futures.ThreadPoolExecutor(max_workers=256) as executor:
            # Start the load operations and mark each future with its repo
            future_to_repo = {executor.submit(cls.get, repo): repo for repo in repos}
            for future in concurrent.futures.as_completed(future_to_repo):
                repo = future_to_repo[future]
                try:
                    future.result()
                except Exception as exc:
                    logging.warning('Repo %r generated an exception: %s' % (repo, exc))

    def __init__(self, repo, data):
        try:
            self.bug_list = data['bug_database']
            self.bug_report = data['bug_submit']
            self.identifier = data['identifier']
            # self.old_path = data['__do_not_use-legacy-projectpath']
            self.repo = data['repo']
            self.artifact = {'group': 5,
                             'branch': 'master'}
            if data['i18n'] and data['i18n']['trunkKF6'] != 'none':
                self.artifact['group'] = 6
                self.artifact['branch'] = data['i18n']['trunkKF6']
        except KeyError as e:
            raise KeyError(f'{repo} :: {repr(data)} :: {e}')
