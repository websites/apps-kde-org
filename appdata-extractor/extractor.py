# SPDX-FileCopyrightText: 2017-2020 Harald Sitter <sitter@kde.org>
# SPDX-FileCopyrightText: 2022 Phu Hung Nguyen <phu.nguyen@kdemail.net>
# SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL

import concurrent.futures
import logging
import os
import requests
import shutil
import subprocess
import tempfile
import traceback

from lib import category
from lib.collectors import AppstreamCollector, GitAppstreamCollector
from lib.icon_fetcher import IconFetcher
from lib.ci_utilities import Registry
from lib.kde_project import Project
from lib.unmaintained import Unmaintained
from lib.xdg.desktop import DesktopDirectoryLoader
from lib.xdg.icon import IconTheme


def select_retrieve_install(p_repo, dest):
    p = Project.get(p_repo)
    registry = suse_qt5_registry if p.artifact['group'] == 5 else suse_qt6_registry
    registry.retrieve_install(p.identifier, dest, p.artifact['branch'])

def notify(repo):
    # send a message to notify about problematic failing build
    # - access token must be available (i.e. on master only)
    if 'MATRIX_ACCESS_TOKEN' not in os.environ:
        return
    homeserver = 'https://kde.modular.im'
    # kde-www room
    room_id = '!KWPxbUviPXLYlJAlCm:matrix.org'
    body = f'[apps.kde.org] failed build: appdata-extractor had problems with project {repo!r}'
    logging.info(f'Sending message to Matrix to notify about problematic projects:\n{body}')

    res = requests.post(f'{homeserver}/_matrix/client/r0/rooms/{room_id}/send/m.room.message',
                        headers={"Authorization": f"Bearer {os.environ['MATRIX_ACCESS_TOKEN']}"},
                        json={"body": body, "msgtype": "m.notice"})
    logging.info(f'Matrix response: {res.json()}')

if __name__ == "__main__":
    level = logging.INFO
    logging.basicConfig(format='%(levelname)s: %(message)s', level=level)

    repos = Project.list_repos()
    filtered = []

    # repos starting with one of these are excluded
    starting_strings_non_apps = ('documentation/', 'frameworks/',
                                 'historical/', 'kde-linux/', 'libraries/',
                                 'neon/', 'network/kaccounts-', 'network/ktp-',
                                 'packaging/', 'plasma-bigscreen/',
                                 'sysadmin/', 'webapps/', 'websites/',
                                 'wikitolearn/')

    # specific repos of apps in each group
    # many Maui apps are not yet ready
    maui_apps = {'index-fm', 'nota', 'pix', 'vvave'}
    pim_apps = {'akregator', 'itinerary', 'kaddressbook', 'kalarm', 'merkuro', 'kitinerary', 'kjots',
                'kleopatra', 'kmail', 'knotes', 'kontact', 'korganizer', 'ktimetracker',
                'pim-data-exporter', 'trojita', 'vakzination', 'zanshin'}
    # Plasma has its own web page, include only some projects
    plasma_apps = {'discover', 'plasma-systemmonitor', 'ksysguard'}
    plasma_mobile_apps = {'calindori',
                          'plasma-camera', 'plasma-dialer', 'plasma-phonebook', 'qmlkonsole',
                          'spacebar'}
    # only rolisteam-packaging contains an app
    rolisteam_apps = {'rolisteam-packaging'}
    system_apps = {'apper', 'dolphin', 'kcron', 'khelpcenter', 'ksystemlog', 'kup', 'liquidshell', 'muon',
                   'partitionmanager', 'wacomtablet', 'kjournald', 'systemdgenie'}
    opted_in_app_groups = {'maui/': maui_apps, 'pim/': pim_apps,
                           'plasma/': plasma_apps, 'plasma-mobile/': plasma_mobile_apps,
                           'rolisteam/': rolisteam_apps, 'system/': system_apps}

    # individual repos of non-apps
    plasmoids = {'alkimia'}
    # libraries: analitza,
    # actual non-apps: atelier, kdialog, print-manager,
    # archived repos: totalreqall,
    # WIP: arkade,
    repos_non_apps = {'analitza', 'arkade', 'atelier', 'kdialog',
                      'print-manager',
                      'totalreqall'} | plasmoids
    # individual ids of non-apps: amarok, keurocalc
    non_apps = {'org.kde.amarok.albums', 'org.kde.amarok.photos', 'org.kde.curconvd'}
    # for testing
    testing_apps = {'ghostwriter'}

    # filter repos
    for repo in repos:
        # some groups are excluded altogether
        if repo.startswith(starting_strings_non_apps):
            continue
        repo_basename = os.path.basename(repo)

        should_skip = False
        for repo_start, included_basenames in opted_in_app_groups.items():
            if repo.startswith(repo_start) and repo_basename not in included_basenames:
                should_skip = True
                break
        if should_skip:
            continue

        # We do want some unmaintained bits for backwards compatibility
        #   hidden away at https://apps.kde.org/unmaintained/
        if repo_basename in Unmaintained.projects_w_repo:
            filtered.append(repo)
            continue
        elif repo.startswith('unmaintained/'):
            continue
        # individual non-apps, data repos and lib repos
        if repo_basename in repos_non_apps or repo_basename.endswith('-data') or repo_basename.startswith('lib'):
            continue

        # if repo_basename not in testing_apps:
        #     continue
        # other than those, include all
        filtered.append(repo)
    repos = filtered + Unmaintained.projects_wo_repo
    Project.gen_cache(repos)

    GITLAB_INSTANCE = 'https://invent.kde.org'
    SUSE_QT5_PROJECT = 'teams/ci-artifacts/suse-qt5.15'
    SUSE_QT6_PROJECT = 'teams/ci-artifacts/suse-qt6.8'
    SUSE_QT5_CACHE_PATH = '/mnt/artifacts/suse-qt5.15/' if os.environ.get('CI_COMMIT_REF_NAME') else '/srv/archives/'
    SUSE_QT6_CACHE_PATH = '/mnt/artifacts/suse-qt6.8/' if os.environ.get('CI_COMMIT_REF_NAME') else '/srv/archives/'
    # SUSE_QT5_CACHE_PATH = SUSE_QT6_CACHE_PATH = 'archives'

    suse_qt5_registry = Registry(SUSE_QT5_CACHE_PATH, GITLAB_INSTANCE, SUSE_QT5_PROJECT)
    suse_qt6_registry = Registry(SUSE_QT6_CACHE_PATH, GITLAB_INSTANCE, SUSE_QT6_PROJECT)

    # Download breeze-icons for getting icons and plasma-workspace for getting app menu categories
    SUPPORTS = {'frameworks/breeze-icons', 'plasma/plasma-workspace'}

    for path in SUPPORTS:
        basename = path.split('/')[1]
        if os.path.isdir(basename):
            continue
        select_retrieve_install(path, basename)

    for path in SUPPORTS:
        basename = path.split('/')[1]
        if not os.path.isdir(basename):
            raise Exception(f'Failed to get {basename}! Resolution of CI artifacts failed!')

    local_breeze = IconTheme('breeze', ['extra-icons', 'breeze-icons/share/icons'])

    for cat, desktop_id in {**category.CATEGORY_DESKTOPS_MAP, **category.SUBCATEGORY_DESKTOPS_MAP}.items():
        loader = DesktopDirectoryLoader(desktop_id, ['plasma-workspace/share/desktop-directories'])
        desktop = loader.find()
        if not desktop:
            raise Exception(f'No desktop file for {desktop_id}')
        name = category.to_code(cat)
        icon_name = desktop.desktop_config['Icon']
        if icon_name.endswith('-symbolic'):
            icon_name = icon_name[:-9]
        IconFetcher(icon_name, local_breeze).extend_appdata({}, name, 'categories/')

    # process non-local projects, put the rest into non_local
    non_local = []
    for repo in repos:
        if local_id := Unmaintained.with_local_id(os.path.basename(repo)):
            AppstreamCollector.grab_project_data('.', Project.get(repo), theme=local_breeze, local_id=local_id)
        else:
            non_local.append(repo)
    repos = non_local
    # now repos contains only non-local stuff, we try to get them from CI or Git

    with tempfile.TemporaryDirectory() as tmp_dir:
        def process_project(p_repo: str):
            p = Project.get(p_repo)
            p_tmp_dir = os.path.join(tmp_dir, p.identifier)
            os.makedirs(p_tmp_dir, exist_ok=True)
            logging.info(f'Processing project {p_tmp_dir}')
            try:
                select_retrieve_install(p_repo, p_tmp_dir)
                ci_result = AppstreamCollector.grab_project_data(p_tmp_dir, p, theme=local_breeze, non_apps=non_apps)
            except Exception as p_e:
                logging.warning('Project %r CI processing generated an exception (%s): %s', p_repo, type(p_e), p_e)
                if f'{p_e}' != 'nothing to open':
                    traceback.print_tb(p_e.__traceback__)
            else:
                if ci_result:
                    return repos.remove(p_repo)
            finally:
                shutil.rmtree(p_tmp_dir)

            # after CI processing, skip Git processing with leftover repos in kdevelop group
            if p_repo.startswith('kdevelop/'):
                return repos.remove(p_repo)

            # FIXME: cache and git pull?
            command = ['git', 'clone', '--depth', '1', f'{GITLAB_INSTANCE}/{p_repo}.git', p_tmp_dir]
            logging.info(f'{command}')
            subprocess.run(command)
            if GitAppstreamCollector.grab_project_data(p_tmp_dir, p, theme=local_breeze, non_apps=non_apps):
                repos.remove(p_repo)
            shutil.rmtree(p_tmp_dir)

        with concurrent.futures.ThreadPoolExecutor(max_workers=8) as executor:
            # Mark each future with its project
            future_to_project = {executor.submit(process_project, repo): repo for repo in repos}
            for future in concurrent.futures.as_completed(future_to_project):
                repo = future_to_project[future]
                try:
                    future.result()
                except Exception as e:
                    notify(repo)
                    raise e
                logging.info(f'Processed project {repo}')

    logging.info(f'Not processed: {repos}')
